#!/usr/bin/env bash

set -o nounset
set -o errexit
set -o pipefail

sudo docker run --name dec --rm -i -t --rm -e DISPLAY=${DISPLAY} -v /tmp/.X11-unix:/tmp/.X11-unix decoder $@


