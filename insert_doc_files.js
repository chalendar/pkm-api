"use strict";

const path = require('path');
const MongoClient = require('mongodb').MongoClient;
const assert = require ('assert');
const common = require ('./common');
const fs = require ('fs');
const md2json = require('md-2-json');
const host=process.env.PKMHOST;
const url = 'mongodb://' + host + ':27017';
const dbName = process.argv[2];
const client = new MongoClient(url,{ useUnifiedTopology: true});

function case1 (collection,val) {
		// val = common.normalise(val);
		const { exec } = require('child_process');
		let out_val = val + '.json';
		exec('$API/doc/doc_to_asfm/build/doc_to_asfm -c $API/doc/config.xml ' + val + ' -o ' + out_val,
				 (err, stdout, stderr) => {
						 if (err) {
								 // node couldn't execute the command
								 console.log('error ',err);
								 throw err;
						 }
						 else
						 {
								 let rawdocument = fs.readFileSync(out_val);
								 let document = JSON.parse(rawdocument);
								 val = common.normalise(val);
								 document.filename = val;
								 collection.insertOne(document, function(err, res) {
										 if (err) {
												 console.log('error ',err);
												 throw err;
										 }
										 else {
												 console.log("1 docx document inserted: " + val);
												 client.close();
										 }
								 })}})
}

function case2 (collection,val) {
		const norm_val = common.normalise(val);
		const { exec } = require('child_process');
		// let out_val = val + '.json';
		console.log("val ",val);
		let content = fs.readFileSync(val, 'utf8');
		let res = md2json.parse (content);
		var document = { "filename": norm_val, "filecontent": res };
		// Insert the content into the collection
		collection.insertOne(document, function(err, res) {
				if (err) {
						console.log(err);
						throw err;
				}
				else {
						console.log("1 markdown document inserted");
						client.close();
				}
		});
}

client.connect(function(err) {
		assert.equal(null,err);
		const db = client.db(dbName);
		const collection = db.collection('Documentation');
		
		// The doc files are given as arguments.
		// Get list all files from command arguments, using process.argv

		process.argv.forEach(function (val, index, array) {
				if (index>2)
				{
						// check if val finishes with .md or .docx or .DOCX						
						if (!val.endsWith('.docx') && !val.endsWith('.DOCX') && !val.endsWith('.md'))
						{
								console.log('argument ' + val + ' does not terminate with .md, .docx or .DOCX');
								client.close();
						}
						else {
								// check if file exists
								if (fs.existsSync(val)) {
										// Convert file into JSON
										// 1. Case of a .docx file
										if (val.endsWith('.docx') || val.endsWith('.DOCX')) {
												case1(collection,val);
										}
										else
												// 2. Case of a .md file
												if (val.endsWith('.md')) {
														case2 (collection,val);
												}
								}
								else {
										console.log('file ' +val + ' does not exist');
										client.close()
								}
						}}})						 
})
