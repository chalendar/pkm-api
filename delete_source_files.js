"use strict";

const path = require('path');
const MongoClient = require('mongodb').MongoClient;
const common = require('./common');
const assert = require ('assert');
const fs = require ('fs');

const host=process.env.PKMHOST;
const url = 'mongodb://' + host + ':27017';
const dbName = process.argv[2];
const client = new MongoClient(url,{ useUnifiedTopology: true});

var source_files = "";

client.connect(function(err) {
		assert.equal(null,err);
		const db = client.db(dbName);
		// The source files are given as arguments.

		// get list of files from command arguments, using process.argv
		process.argv.forEach(function (val, index, array) {
				if (index>2)
				{
						val = common.normalise(val);
						console.log(index + ': ' + val);
						// Delete the content from the Rawsourcecode, Sourcecode and Annotations collections
						const collection1 = db.collection('RawSourcecode');
						const collection2 = db.collection('Sourcecode');
						const collection3 = db.collection('Annotations');
						const collection4 = db.collection('Comments');
						collection1.deleteMany({"filename":val, function(err, res) {
								if (err) {
										console.log(err);
										throw err;
								}
								else {
										console.log("rawsourcecode documents deleted");
										// client.close();
								}
						}});
						collection2.deleteMany({"fileName":val, function(err, res) {
								if (err) {
										console.log(err);
										throw err;
								}
								else {
										console.log("sourcecode documents deleted");
										// client.close();
								}
						}});
						// for annotations, the filename is inside the location
						collection3.deleteMany({"loc.pos_start.pos_path":val,
																		function(err, res) {
								if (err) {
										console.log(err);
										throw err;
								}
								else {
										console.log("annotation documents deleted");
										// client.close();
								}
						}});
						collection4.deleteMany({"loc.pos_start.pos_path":val,
																		function(err, res) {
																				if (err) {
																						console.log(err);
																						throw err;
																				}
																				else {
																						console.log("annotation documents deleted");
																						// client.close();
																				}
																		}});
				}
		});
		console.log("All documents have been deleted");
		client.close();
})
