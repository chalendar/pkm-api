#!/usr/bin/env bash

set -o nounset
set -o errexit
set -o pipefail

sudo docker build -t decoder .
