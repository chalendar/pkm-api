conn = new Mongo();
db = connect(arg1 +":27017/"+arg2);

function remove_all_collections () {

    let coll = ["Sourcecode", "Annotations", "RawSourcecode", "Comments", "Documentation", "UMLClasses", "Logs", "ERModels", "NERModels", "NERAnnotations", "TraceabilityMatrix", "TESTARStateModels", "TESTARTestResults" ];
    
    for (let i = 0; i< coll.length; i++) {
				db[coll[i]].drop();
    };
    let res = db.getCollectionInfos();
    // print ("Done, DB has removed all collections. ");
    // printjson(res);
}

remove_all_collections ();
