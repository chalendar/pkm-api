conn = new Mongo();
host = arg1;
db = connect(host + ":27017/"+arg2);

function create_all_collections () {
    // let coll = arguments[0]; arguments does not exist
    let coll = ["Sourcecode", "Annotations", "RawSourcecode", "Comments", "Documentation", "UMLClasses", "Logs", "ERModels", "NERModels", "NERAnnotations", "TraceabilityMatrix", "TESTARStateModels", "TESTARTestResults" ];
    
    for (let i = 0; i< coll.length; i++) {
	db.createCollection(coll[i]);
    };
}

create_all_collections ();
