#!/bin/bash
# validate_and_insert_testar_state_model model files into DB
# dos2unix validate_and_insert_testar_state_model.bash

if [[ $# < "3" ]]; then
    echo "Insufficient arguments found"
    echo "Usage: validate_and_insert_testar_state_model DB_name schema_to_validate document_to_insert "
		echo "see sub-directory schemas for the first argument"
else
		# echo arguments $@
		node $API/validate_and_insert_testar_state_model.js $@
fi
