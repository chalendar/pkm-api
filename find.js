"use strict";
// Find <kind> <name>
// Kind: function, type, variable, tag

var MongoClient = require('mongodb').MongoClient;
const assert = require ('assert');
const fs = require ('fs');
const common = require('./common');
const url = 'mongodb://localhost:27017 ';
const client = new MongoClient(url,{ useUnifiedTopology: true });
const dbName = process.argv[2];

client.connect(function(err) {
		if (err) throw err;
		const db = client.db(dbName);
		let kind = process.argv[3];
		let val = process.argv[4];
		//				let val = process.argv[i];
		if (kind == "function") {
				let collection = db.collection("Sourcecode");
				console.log("Searching AST files for function definition...");
				var element = collection.find({"globals.GFun.fundec.svar.varinfo.vname": val});
				let count = 0;
				element.each( function(err,tmp) {
						if (err) {
								console.log(err);
								throw err;
						}
						else {
								if (tmp == null) {
										client.close()
								}
								else
								{
										count = count+1;
										console.log("> found function definition (" + count + ")");
										console.log("fundec",tmp.globals.GFun.fundec);
										// console.log(JSON.stringify(tmp.globals.GFun.fundec,undefined,2));
										
										// searching for file associated to the current tmp:
										let fname = tmp.globals.GFun.loc.pos_start.pos_path;
										console.log("> located in file DB_ROOT/" + common.normalise(fname) + " (" + count + ")");
										// Find the content of this file
										let rawcollection = db.collection("RawSourcecode");
										rawcollection.createIndex("filename");

										var rawelement = rawcollection.find({"filename":fname});
										console.log("Searching in raw files...");
										
										rawelement.each(function(rawerr,rawtmp) {
												if (rawerr) {
														console.log(rawerr);
														throw rawerr;
												}
												else {
														if (rawtmp == null) {
																client.close()
														}
														else
														{
																console.log("> found raw file DB_ROOT/" + common.normalise(rawtmp.filename));
																client.close();
														}
												}
										})
								}}})
				// Lookup the GFunDecl
				console.log("Searching AST files for function declaration...");
				var element2 = collection.find({"globals.GFunDecl.varinfo.vname": val});
				// var tab2= element2.toArray(function (err,documents) {
				// 		console.log("> found function declaration (" + documents.length+")");
				// });
				let count2 = 0;
				element2.each(function(err2,tmp2) {
						if (err2) {
								console.log(err2);
								throw err2;
						}
						else {
								if (tmp2 == null) {
										client.close()
								}
								else
								{
										count2 = count2 + 1;
										console.log("> found function declaration (" + count2 + ")");
										console.log("funspec",tmp2.globals.GFunDecl.funspec);
										
										// searching for file associated to the current tmp:
										let fname = tmp2.globals.GFunDecl.loc.pos_start.pos_path;
										console.log("> located in file DB_ROOT/" + common.normalise(fname) + " (" + count2 + ")");
										// Find the content of this file
										let rawcollection = db.collection("RawSourcecode");
										var rawelement = rawcollection.find({"filename":fname});
										console.log("> Searching in raw files...");
										rawelement.each(function(rawerr,rawtmp) {
												if (rawerr) {
														console.log(rawerr);
														throw rawerr;
												}
												else {
														if (rawtmp == null) {
																client.close()
														}
														else
														{
																console.log("> found raw file : DB_ROOT/" + common.normalise(rawtmp.filename));
																client.close();
														}
												}
										})
								}}})
		}
		else
				if (kind == "variable") {
						let collection = db.collection("Sourcecode");
						console.log("Searching AST files for variable definition...");
						var element = collection.find({"globals.GVar.varinfo.vname": val});
						let count = 0;
						element.each( function(err,tmp) {
								if (err) {
										console.log(err);
										throw err;
								}
								else {
										if (tmp == null) {
												client.close()
										}
										else
										{
												count = count+1;
												console.log("> found definition (" + count + ")");
												console.log("varinfo",tmp.globals.GVar);
												
												// searching for file associated to the current tmp:
												let fname = tmp.globals.GVar.loc.pos_start.pos_path;
												console.log("> located in file DB_ROOT/" + common.normalise(fname) + " (" + count + ")");
												// Find the content of this file
												let rawcollection = db.collection("RawSourcecode");
												rawcollection.createIndex("filename");
												var rawelement = rawcollection.find({"filename":fname});
												console.log("Searching in raw files...");
												
												rawelement.each(function(rawerr,rawtmp) {
														if (rawerr) {
																console.log(rawerr);
																throw rawerr;
														}
														else {
																if (rawtmp == null) {
																		client.close()
																}
																else
																{
																		console.log("> found raw file DB_ROOT/" + common.normalise(rawtmp.filename));
																		client.close();
																}
												}
										})
										}}})
						// Lookup the GVarDecl
						console.log("Searching AST files for variable declaration...");
						var element2 = collection.find({"globals.GVarDecl.varinfo.vname": val});
						let count2 = 0;
						element2.each(function(err2,tmp2) {
								if (err2) {
										console.log(err2);
										throw err2;
								}
								else {
										if (tmp2 == null) {
												client.close()
										}
										else
										{
												count2 = count2 + 1;
												console.log("> found variable declaration (" + count2 + ")");
												console.log("varinfo",tmp.globals.GVarDecl.varinfo);
												
												// searching for file associated to the current tmp:
												let fname = tmp2.globals.GVarDecl.loc.pos_start.pos_path;
												console.log("> located in file DB_ROOT/" + common.normalise(fname) + " (" + count2 + ")");
												// Find the content of this file
												let rawcollection = db.collection("RawSourcecode");
												var rawelement = rawcollection.find({"filename":fname});
												console.log("> Searching in raw files...");
												rawelement.each(function(rawerr,rawtmp) {
														if (rawerr) {
																console.log(rawerr);
																throw rawerr;
														}
														else {
																if (rawtmp == null) {
																		client.close()
																}
																else
																{
																		console.log("> found raw file DB_ROOT/" + common.normalise(rawtmp.filename));
																		client.close();
																}
														}
												})
										}}})
				}
		else 
				if (kind == "tag") {
						let collection = db.collection("Sourcecode");
						console.log("Searching AST files for tag (Comp/Enum) definition...");
						var element = collection.find({"globals.GCompTag.compinfo.cname": val});
						let count = 0;
						element.each( function(err,tmp) {
								if (err) {
										console.log(err);
										throw err;
								}
								else {
										if (tmp == null) {
												client.close()
										}
										else
										{
												count = count+1;
												console.log("> found tag definition (" + count + ")");
												console.log("compinfo",tmp.globals.GCompTag.compinfo);
												
												// searching for file associated to the current tmp:
												let fname = tmp.globals.GCompTag.loc.pos_start.pos_path;
												console.log("> located in file DB_ROOT/" + common.normalise(fname) + " (" + count + ")");
												// Find the content of this file
												let rawcollection = db.collection("RawSourcecode");
												rawcollection.createIndex("filename");
												
												var rawelement = rawcollection.find({"filename":fname});
												console.log("Searching in raw files...");
												
												rawelement.each(function(rawerr,rawtmp) {
														if (rawerr) {
																console.log(rawerr);
																throw rawerr;
														}
														else {
																if (rawtmp == null) {
																		client.close()
																}
																else
																{
																		console.log("> found raw file DB_ROOT/" + rawtmp.filename);
																		client.close();
																}
														}
												})
										}}})
						// Lookup the GCompTagDecl
						console.log("Searching AST files for tag declaration...");
						var element2 = collection.find({"globals.GCompTagDecl.compinfo.cname": val});
						let count2 = 0;
						element2.each(function(err2,tmp2) {
								if (err2) {
										console.log(err2);
										throw err2;
								}
								else {
										if (tmp2 == null) {
												client.close()
										}
										else
										{
												count2 = count2 + 1;
												console.log("> found tag declaration (" + count2 + ")");
												console.log("compinfo",tmp.globals.GCompTagDecl.compinfo);
												
												// searching for file associated to the current tmp:
												let fname = tmp2.globals.GFunDecl.loc.pos_start.pos_path;
												console.log("> located in file DB_ROOT/" + common.normalise(fname) + " (" + count2 + ")");
												// Find the content of this file
												let rawcollection = db.collection("RawSourcecode");
												var rawelement = rawcollection.find({"filename":fname});
												console.log("> Searching raw files...");
												rawelement.each(function(rawerr,rawtmp) {
														if (rawerr) {
																console.log(rawerr);
																throw rawerr;
														}
														else {
																if (rawtmp == null) {
																		client.close()
																}
																else
																{
																		console.log("> found raw file DB_ROOT/" + rawtmp.filename);
																		client.close();
														}
														}
												})
										}}})
				}
		else {
				console.log("kind not found");
				client.close();
		}
});
