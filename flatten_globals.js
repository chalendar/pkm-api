"use strict";

// Arguments: arg1 is the user name , arg2 is the DB name

const path = require('path');
const MongoClient = require('mongodb').MongoClient;
const assert = require ('assert');
const fs = require ('fs');
const common = require('./common');

const host=process.env.PKMHOST;
const url = 'mongodb://'+ host + ':27017';
const dbName = process.argv[2];
const client = new MongoClient(url,{ useUnifiedTopology: true});

client.connect(function(err) {
		assert.equal(null,err);
		const db = client.db(dbName);
		const collection = db.collection('Sourcecode');




		collection.aggregate ([{$match:{}},{$unwind : "$globals"}]).toArray(
				function(err,data) {
						if (err) {
								console.log("error");
								throw err;
						}
						else {
								// console.log(JSON.stringify(data).length);
								if(data.length > 0){
										// console.log(data);
										// it is a list
										// Delete old elements 
										collection.deleteMany({});
										// insert all new ones
										for (let i=0; i<data.length;i++) {
												let tmp = data[i];
												let ffilename = "";
												// get the proper value of the file name that is in every
												// global:
												let filename=null;
												let glob1 = tmp.globals.GType;
												if (glob1!=null) {
														// print("GType");
														ffilename = tmp.globals.GType.loc.pos_start.pos_path;
												};
												glob1 = tmp.globals.GCompTag;
												if (glob1!=null) {
														// print("GCompTag");
														ffilename = tmp.globals.GCompTag.loc.pos_start.pos_path;
												};
												glob1 = tmp.globals.GCompTagDecl;
												if (glob1!=null) {
														// print("GCompTagDecl");
														ffilename = tmp.globals.GCompTagDecl.loc.pos_start.pos_path;
												};
												glob1 = tmp.globals.GEnumTag;
												if (glob1!=null) {
														// print("GEnumtag");
														ffilename = tmp.globals.GEnumTag.loc.pos_start.pos_path;
												};
												glob1 = tmp.globals.EnumTagDecl;
												if (glob1!=null) {
														// print("GEnumTagDecl");
														ffilename = tmp.globals.GEnumTagDecl.loc.pos_start.pos_path;
												};
												glob1 = tmp.globals.VarDecl;
												if (glob1!=null) {
														// print("GVarDecl");
														ffilename = tmp.globals.GVarDecl.loc.pos_start.pos_path;
												};
												glob1 = tmp.globals.GFunDecl;
												if (glob1!=null) {
														// print("GFunDecl");
														ffilename = tmp.globals.GFunDecl.loc.pos_start.pos_path;
												};
												glob1 = tmp.globals.GVar;
												if (glob1!=null) {
														// print("GVar");
														ffilename = tmp.globals.GVar.loc.pos_start.pos_path;
												};
												glob1 = tmp.globals.GFun;
												if (glob1!=null) {
														// print("GFun " + tmp.globals.GFun.fundec.svar.varinfo.vname);
														ffilename =  tmp.globals.GFun.loc.pos_start.pos_path;
												};
												glob1 = tmp.globals.GAsm;
												if (glob1!=null) {
														// print("GAsm");
														ffilename = tmp.globals.GAsm.loc.pos_start.pos_path;
												};
												glob1 = tmp.globals.GPragma;
												if (glob1!=null) {
														// print("GPragma");
														ffilename = tmp.globals.GPragma.loc.pos_start.pos_path;
												};
												glob1 = tmp.globals.GText;
												if (glob1!=null) {
														// print("GText");
														ffilename = tmp.globals.GText.loc.pos_start.pos_path;
												};
												glob1 = tmp.globals.GAnnot;
												if (glob1!=null) {
														// print("GAnnot");
														ffilename = tmp.globals.GAnnot.loc.pos_start.pos_path;
												};
												
												if (ffilename!=null) {
														//print("inserting from " + ffilename + " element no " + count);
														let gglobals = tmp.globals;
														collection.insertOne ({"fileName": common.normalise(ffilename),
																									 "globals":gglobals});
												};
												
										};
										client.close();
								}
								else{
										console.log("error : Data does not exist.");
										client.close();
								}
						}   
				}
		)
})

