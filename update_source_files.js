"use strict";

const path = require('path');
const MongoClient = require('mongodb').MongoClient;
const assert = require ('assert');
const fs = require ('fs');
const common = require('./common');

const host = process.env.PKMHOST;
const url = 'mongodb://' + host + ':27017';
const dbName = process.argv[2];
const client = new MongoClient(url,{ useUnifiedTopology: true});

var source_files = "";

function get_filename(tmp) {
		var ffilename=null;
		var glob1 = tmp.GType;
		if (glob1!=null) {
				// console.log("GType");
				ffilename = tmp.GType.loc.pos_start.pos_path;
		};
		glob1 = tmp.GCompTag;
		if (glob1!=null) {
				// console.log("GCompTag");
				ffilename = tmp.GCompTag.loc.pos_start.pos_path;
		};
		glob1 = tmp.GCompTagDecl;
		if (glob1!=null) {
				// console.log("GCompTagDecl");
				ffilename = tmp.GCompTagDecl.loc.pos_start.pos_path;
		};
		glob1 = tmp.GEnumTag;
		if (glob1!=null) {
				// console.log("GEnumtag");
				ffilename = tmp.GEnumTag.loc.pos_start.pos_path;
		};
		glob1 = tmp.EnumTagDecl;
		if (glob1!=null) {
				// console.log("GEnumTagDecl");
				ffilename = tmp.GEnumTagDecl.loc.pos_start.pos_path;
		};
		glob1 = tmp.VarDecl;
		if (glob1!=null) {
				// console.log("GVarDecl");
				ffilename = tmp.GVarDecl.loc.pos_start.pos_path;
		};
		glob1 = tmp.GFunDecl;
		if (glob1!=null) {
				// console.log("GFunDecl");
				ffilename = tmp.GFunDecl.loc.pos_start.pos_path;
		};
		glob1 = tmp.GVar;
		if (glob1!=null) {
				// console.log("GVar");
				ffilename = tmp.GVar.loc.pos_start.pos_path;
		};
		glob1 = tmp.GFun;
		if (glob1!=null) {
				// console.log("GFun");
				ffilename = tmp.GFun.loc.pos_start.pos_path;
		};
		glob1 = tmp.GAsm;
		if (glob1!=null) {
				// console.log("GAsm");
				ffilename = tmp.GAsm.loc.pos_start.pos_path;
		};
		glob1 = tmp.GPragma;
		if (glob1!=null) {
				// console.log("GPragma");
				ffilename = tmp.GPragma.loc.pos_start.pos_path;
		};
		glob1 = tmp.GText;
		if (glob1!=null) {
				// console.log("GText");
				ffilename = tmp.GText.loc.pos_start.pos_path;
		};
		glob1 = tmp.GAnnot;
		if (glob1!=null) {
				// console.log("GAnnot");
				ffilename = tmp.GAnnot.loc.pos_start.pos_path;
		};
		return ffilename;
}

function update_and_flatten_documents (doc, collection, annot, annot_collection, comments, comments_collection) {
		// flatten an AST document by disaggregating the list of globals
		// these are located in the 1st level 'globals" field
		var nb_globals = doc.globals.length;
		console.log("number of globals to insert "+nb_globals);

		// first delete all globals that will be replaced by new ones
		for (let i=0; i<nb_globals;i++) {
				fname =  get_filename(doc.globals[i]);
				collection.deleteMany({"fileName":fname});
		};
		// then insert the new ones
		for (let i=0; i<nb_globals;i++) {
				var fname = get_filename(doc.globals[i]);
				var global = doc.globals[i]
				var new_doc = {"fileName":fname, "globals": global };
				// console.log("inserting global ", new_doc);
				collection.insertOne(new_doc, function(err, res) {
						if (err) {
								console.log(err);
								throw err;
						}
						else {
								// console.log("SOURCE document inserted: " + new_doc);
								// client.close();
						}
				});	
		};

		// update annotations collection
		var nb_annot = annot.annotations.length;
		for (let i=0; i<nb_annot;i++) {
				fname =  annot.annotations[i].loc.pos_start.pos_path;
				annot_collection.deleteMany({"fileName":fname});
		};
		console.log("number of annotations to update ", nb_annot);
		for (let j=0; j<annot.annotations.length; j++) {
				var fic = annot.annotations[j].loc.pos_start.pos_path;
				annot_collection.insertOne(annot.annotations[j],
																		 function(err, res) {
																				 if (err) {
																						 console.log(err);
																						 throw err;
																				 }
																				 else {
																						 // console.log('updated annotation ',annot.annotations[j]);
																				 };
																		 
																		 })
		}

		// update comments collection	 
		var nb_comments = comments.comments.length;
		for (let i=0; i<nb_comments;i++) {
				fname =  comments.comments[i].loc.pos_start.pos_path;
				comments_collection.deleteMany({"fileName":fname});
		};
		console.log("number of comments to update ", nb_comments);
		for (let i=0; i<comments.comments.length; i++) {										
				var fic = comments.comments[i].loc.pos_start.pos_path;
				comments_collection.insertOne(comments.comments[i],
																			function(err, res) {
																					if (err) {
																							console.log(err);
																							throw err;
																					}
																					 else {
																							 // console.log('updated comment ',comments.comments[i]);
																					 };
																			});
		}
};

client.connect(function(err) {
		const db = client.db(dbName);
		assert.equal(null,err);
		// The source files are given as arguments.
		// Update the content of the files as strings
		// into the Rawsourcecode collection:

		// get list of files from command arguments, using process.argv
		let source_files = "";
		process.argv.forEach(function (val, index, array) {
				if (index>2)
				{
						source_files = source_files + " " + val ; // catenation
						// Get content of the file 'val':
						var content = "content";
						let decoder = new TextDecoder();

						content = fs.readFileSync(val);
						// console.log("content : " + content);
						// decode the buffer and get a string
						let content_string = decoder.decode(content);
						val = common.normalise(val);
						var document = { "filename": val, "filecontent": content_string };
						var short_document = {"filecontent": content_string };
						// console.log(document);
						// Update the content into the Rawsourcecode collection
						const collection = db.collection('RawSourcecode');
						collection.replaceOne({"filename":val},
																	{$set: document},
																	{"upsert":true, "overwrite":true},
																	function(err, res) {
																			if (err) {
																					console.log(err);
																					throw err;
																			}
																			else {
																					// console.log("1 rawsourcecode document updated");
																					// client.close();
																			}
																	});
				}
		});
		
		// Insert the frama-c-processed AST into another collection

		// First call frama-c on the list of source files
		const { exec } = require('child_process');
		exec('frama-c -keep-comments -json -json-out result.json'
				 + source_files, (err, stdout, stderr) => {
						 if (err) {
								 // node couldn't execute the command
								 console.log(err);
								 throw err;
						 }
						 else
						 {
								 // the *entire* stdout and stderr (buffered) are output
								 console.log(`Frama-C stdout: ${stdout}`);
								 console.log(`Frama-C stderr: ${stderr}`);

								 // get the JSON result from file result.json
								 let rawdata = fs.readFileSync('result.json');
								 let code = JSON.parse(rawdata);
								 const collection = db.collection('Sourcecode');
								 
								 let rawannot = fs.readFileSync('annot_result.json');
								 let annot = JSON.parse(rawannot);
								 const annot_collection = db.collection('Annotations');

								 let rawcomments = fs.readFileSync('comments_result.json');
								 let comments = JSON.parse(rawcomments);
								 const comments_collection = db.collection('Comments');
								 
								 // insert all 3 documents in their respective collection
								 update_and_flatten_documents(code, collection, annot, annot_collection, comments, comments_collection);
								 client.close();
						 }
				 });
});
