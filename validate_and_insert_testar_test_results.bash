#!/bin/bash
# validate_and_insert_testar_test_results DB_name model files
# dos2unix validate_and_insert_testar_test_results.bash


if [[ $# < "3" ]]; then
    echo "Insufficient arguments found"
    echo "Usage: validate_and_insert_testar_test_results DB_name schema_to_validate document_to_insert "
else
		# echo arguments $@
		node $API/validate_and_insert_testar_test_results.js $@
fi
