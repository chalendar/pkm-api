conn = new Mongo();
host = arg1;
db = connect(host + ":27017/"+arg2);

function create (role) {
		try {
				db.createRole(role);
		}
		catch (err) {};
};
    var feeder = {
				"role": "feeder",
				"privileges": [
						{"resource":
						 {"db": arg2, "collection": ""},
             "actions": 
             ["collStats", "convertToCapped", "dbHash", "dbStats", "find", "insert", "killCursors", "listIndexes", "listCollections", "remove", "update"]
            }
				],
				"roles":[]
    };

    var supervisor = {
				"role": "supervisor",
				"privileges": [
						{"resource":
						 {"db": arg2, "collection": ""},
             "actions": 
             ["collStats",
							"insert",
							"remove",
							"update",
							"changeCustomData",
							"changePassword",
							"createRole",
							"createUser",
							"dropRole",
							"dropUser",
							"grantRole",
							"revokeRole",
							"setAuthenticationRestriction",
							"viewRole",
							"viewUser",
							"changeStream",
							"collStats",
							"convertToCapped",
							"createCollection",
							"dbHash",
							"dbStats",
							"dropCollection",
							"find",
							"killCursors",
							"listCollections",
							"listIndexes",
							"planCacheRead"
						 ]
            }
				],
				"roles":[]
    };
    
    var reader = {
				"role": "reader",
				"privileges": [
						{"resource":
						 {"db": arg2, "collection": ""},
             "actions": 
             ["collStats", "convertToCapped", "dbHash", "dbStats", "killCursors", "listIndexes", "listCollections"]
            }
				],
				"roles":[]
    };
    
    var reviewer =
				{ "role": "reviewer",
					"privileges": [
							{"resource":
							 {"db": arg2, "collection": ""},
               "actions": 
               ["collStats", "convertToCapped", "dbHash", "dbStats", "find", "insert", "killCursors", "listIndexes", "listCollections", "remove", "update"]
              }
					],
					"roles":[]
				};

var developer =
				{ "role": "developer",
					"privileges": [
							{"resource":
							 {"db": arg2, "collection": ""},
               "actions": 
               ["collStats", "convertToCapped", "dbHash", "dbStats", "find", "insert", "killCursors", "listIndexes", "listCollections", "remove", "update"]
              }
					],
					"roles":[]
				};

create(feeder);
create(supervisor);
create(reader);
create(developer);
create(reviewer);
    
