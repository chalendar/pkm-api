// "use strict";

const path = require('path');
const MongoClient = require('mongodb').MongoClient;
const assert = require ('assert');
const fs = require ('fs');
const host=process.env.PKMHOST;
const url = 'mongodb://' +host +':27017';
const dbName = process.argv[2];
const client = new MongoClient(url,{ useUnifiedTopology: true});


client.connect(function(err) {
		assert.equal(null,err);
		const db = client.db(dbName);
		// The model files are given as arguments.

		// Get list of files from command arguments, using process.argv

		process.argv.forEach(function (val, index, array) {
				if (index>2)
				{
						// console.log(index + ': ' + val);
						let rawdocument = fs.readFileSync(val);
						let document = JSON.parse(rawdocument);
						console.log("fresh document " + document);
						// We can complicate the document by adding extra fields before insertion
						const collection = db.collection('UMLClasses');
						// Please add filter below (1st argument)
						collection.replaceOne({},
																	{$set: document},
																	{"upsert":true, "overwrite":true},
																	function(err, res) {
																			if (err) {
																					console.log(err);
																					throw err;
																			}
																			else {
																					console.log("Model document updated: " + val);
																					client.close();
																			}
																	});
				}
		});
		console.log("All model files have been updated.");
});
