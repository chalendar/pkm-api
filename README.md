# README

This repository contains the **PKM API source code**.

It is meant to be installed once MongoDB server V3.4 once it has been installed properly.
The installation guide for the server is in deliverable D1.3. It can be found on Nextcloud at
https://nextcloud.ow2.org/index.php/apps/files/?dir=/SHARED_WITH_ME/DECODER%20Collaborative%20Editing/WP1%20Persistent%20Knowledge%20Monitor/D1.3&fileid=18568


The API installation and usage manual are in deliverable D1.2 that is on Nextcloud at location
https://nextcloud.ow2.org/index.php/apps/files/?dir=/SHARED_WITH_ME/DECODER%20Collaborative%20Editing/WP1%20Persistent%20Knowledge%20Monitor/D1.2&fileid=18567
Please read section 6.

Currently, both parts run on Windows 10, MacOS X and Ubuntu 18.04.

**Note to API developers**: this repository uses a sub-module, api_to_doc, which is compiled when running the API function initialise_PKM.
Developers using other functions of this sub-modules ar when updating to a higher version if it, please perform:

$ git submodule init

$ git submodule update


## pkm-api installation notes in docker

You should previously have a docker installation on your machine.

For ubuntu:

```sh
sudo apt install docker.io
sudo systemctl start docker
sudo systemctl enable docker
xhost +local:docker
```

or

```sh
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install docker-ce
xhost +local:docker
```

To create a docker image of decoder (be prepared to wait for a while: ~10mns)

```
sudo docker build -t decoder .
```

It automatically runs all the commands of `DockerFile` inside the image.
Then the command

```
docker run --name dec --rm -i -t --rm -e DISPLAY=${DISPLAY} -v /tmp/.X11-unix:/tmp/.X11-unix decoder bash
```

gives an access to the machine.

It is possible to run a particular program in docker 
```
docker exec -it dec /root/.opam/4.08.1/bin/frama-c-gui
```


