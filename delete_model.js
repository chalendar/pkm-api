"use strict";

const MongoClient = require('mongodb').MongoClient;
const assert = require ('assert');
const path = require ('path');
const fs = require ('fs');
const host=process.env.PKMHOST;
const url = 'mongodb://' + host +':27017';
const dbName = process.argv[2];
const client = new MongoClient(url,{ useUnifiedTopology: true});

var source_files = "";

client.connect(function(err) {
		assert.equal(null,err);
		const db = client.db(dbName);
		// The models to be deleted are given as arguments.

		// get list of models from command arguments, using process.argv
		process.argv.forEach(function (val, index, array) {
				if (index>2)
				{
						// Delete the content from the UMLClasses collection
						const collection1 = db.collection('UMLClasses');
						collection1.deleteMany({"name":val, function(err, res) {
								if (err) {
										console.log(err);
										throw err;
								}
								else {
										console.log("1 model deleted");
										// client.close();
								}
						}});
				}
		});
		console.log("All models have been deleted");
		client.close();
})
