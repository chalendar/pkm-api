"use strict";

const path = require('path');
const assert = require ('assert');
const fs = require ('fs');
const md2json = require('md-2-json');

var val = process.argv[2];
var out_val = process.argv[3];
if (val.endsWith('.md')) {
		let content = fs.readFileSync(val, 'utf8');
		let res = md2json.parse (content);
		fs.writeFileSync(out_val,JSON.stringify(res));
}
else {
		console.log('Wrong file suffix, shall be .md')
}

					 
				 
