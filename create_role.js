conn = new Mongo();

db = connect(arg4 + ":27017/"+arg2);

var new_role =
		{ "role": arg1,
			"privileges": [
					{"resource":
					 {"db": "mydb", "collection": ""},
           "actions": arg3
          }
			],
			"roles":[]
		};

db.createRole(new_role);
