"use strict";

const path = require('path');
const MongoClient = require('mongodb').MongoClient;
const assert = require ('assert');
const fs = require ('fs');
const common = require('./common');

const host=process.env.PKMHOST;
const url = 'mongodb://'+ host + ':27017';
const dbName = process.argv[2];
const client = new MongoClient(url,{ useUnifiedTopology: true});

var source_files = "";

client.connect(function(err) {
		assert.equal(null,err);
		const db = client.db(dbName);
		// The source files are given as arguments.
		// Insert the content of the files as strings
		// into the Rawsourcecode collection:

		// get list of files from command arguments, using process.argv
		let source_files = "";
		process.argv.forEach(function (val, index, array) {
				if (index>2)
				{
						source_files = source_files + " " + val ; // catenation
						// Get content of the file 'val':
						var content = "content";
						let decoder = new TextDecoder();
						content = fs.readFileSync(val);
						// decode the buffer and get a string
						let content_string = decoder.decode(content);
						// Compute absolute file namme of val:
						val = common.normalise(val);
						var document = { "filename": val, "filecontent": content_string };
						// Insert the content into the Rawsourcecode collection
						const collection = db.collection('RawSourcecode');
						collection.insertOne(document, function(err, res) {
								if (err) {
										console.log(err);
										throw err;
								}
								else {
										console.log("1 rawsourcecode document inserted");
								}
						});
				}
		});
		console.log("All source files have been inserted");
		
		// Insert the frama-c-processed AST into another collection
		
		// First call frama-c on the list of source files
		const { exec } = require('child_process');
		exec('frama-c -keep-comments -json -json-out result.json'
				 + source_files, (err, stdout, stderr) => {
						 if (err) {
								 console.log(err);
								 throw err;
						 }
						 else
						 {
								 // the *entire* stdout and stderr (buffered) are output
								 console.log(`stdout: ${stdout}`);
								 console.log(`stderr: ${stderr}`);
								 // get the JSON result from file result.json
								 let rawdata = fs.readFileSync('result.json');
								 let decoder = new TextDecoder();
								 let content_string = decoder.decode(rawdata);
								 // replace absolute paths by normalised relative paths
								 let content_improved = common.replace(content_string);
								 let code = JSON.parse(content_improved);
								 // insert them into the Sourcecode collection
								 const collection = db.collection('Sourcecode');
								 collection.insertOne(code, function(err, res) {
										 if (err) {
												 console.log(err);
												 throw err;
										 }
										 else {
												 // console.log("1 sourcecode document inserted");
												 client.close();
										 };
								 });

								 // check if empty file
								 fs.access('annot_result.json', fs.constants.F_OK, (err) => {
										 if (err) {
												 console.error(err)
												 throw err
										 }
										 else {
												 fs.stat("annot_result.json", function(err2,stats) {
														 if (err2) {
																 console.log(err2);
																 throw err2;
														 }
														 else {
																 if (stats.size >0) {
																		 let rawannot = fs.readFileSync('annot_result.json');
																		 let decoder = new TextDecoder();
																		 let content_string2 = decoder.decode(rawannot);
																		 // replace absolute paths by normalised relative paths
																		 let content_improved2 = common.replace(content_string2);
																		 let annot = JSON.parse(content_improved2);
																		 // console.log(JSON.stringify(annot));
																		 const annot_collection = db.collection('Annotations');
																		 for (let i=0; i<annot.annotations.length; i++) {
																				 annot_collection.insertOne(annot.annotations[i], function(err3, res) {
								 														 if (err3) {
								 																 console.log(err3);
								 																 throw err3;
								 														 }
								 														 else {
								 																 client.close();
								 														 };
																				 });
																		 };
																 }
														 }
												 })}
								 });
								 
								 // check if empty file
								 fs.access('comments_result.json', fs.constants.F_OK, (err) => {
										 if (err) {
												 console.error(err)
												 throw err;
										 }
										 else {
												 fs.stat("comments_result.json", function(err2,stats) {
														 if (err2) {
																 console.log(err2);
																 throw err2;
														 }
														 else {
																 if (stats.size >0) {
																		 let rawcomments = fs.readFileSync('comments_result.json');
																		 let decoder = new TextDecoder();
																		 let content_string3 = decoder.decode(rawcomments);
																		 // replace absolute paths by normalised relative paths
																		 let content_improved3 = common.replace(content_string3);
																		 let comments = JSON.parse(content_improved3);
																		 const comments_collection = db.collection('Comments');
																		 for (let i=0; i<comments.comments.length; i++) {
																				 comments_collection.insertOne(comments.comments[i], function(err3, res) {
								 														 if (err3) {
								 																 console.log(err3);
								 																 throw err3;
								 														 }
								 														 else {
								 																 client.close();
								 														 };
																				 });
																		 }
																 }
														 }
												 })
										 }});
						 }})
})
