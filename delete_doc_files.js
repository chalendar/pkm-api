"use strict";

const MongoClient = require('mongodb').MongoClient;
const assert = require ('assert');
const path = require ('path');
const fs = require ('fs');
const common = require ('./common.js');

const host=process.env.PKMHOST;
const url = 'mongodb://' + host + ':27017';
const dbName = process.argv[2];
const client = new MongoClient(url,{ useUnifiedTopology: true});

var source_files = "";

client.connect(function(err) {
		assert.equal(null,err);
		const db = client.db(dbName);
		// The doc files are given as arguments.

		// get list of files from command arguments, using process.argv
		process.argv.forEach(function (val, index, array) {
				if (index>2)
				{
						// console.log(index + ': ' + val);
						
						// Delete the content from the Documentation collection
						const collection1 = db.collection('Documentation');
						// check if val is an absolute or relative path. In the second case, get the absolute name first:
						val = common.normalise (val);
						collection1.deleteMany({"filename":val, function(err, res) {
								if (err) {
										console.log(err);
										throw err;
								}
								else {
										console.log("1 doc file deleted");
										// client.close();
								}
						}});
				}
		});
		console.log("All doc files have been deleted");
		client.close();
})
