conn = new Mongo();
db = connect(arg3 + ":27017/"+arg2);

user1 = arg1 + "_feeder";
user2 = arg1 + "_reader";
user3 = arg1 + "_supervisor";
user4 = arg1 + "_developer";
user5 = arg1 + "_reviewer";
user6 = arg1 + "_admin";
user7 = arg1 + "_owner"

db.dropUser(user1);
db.dropUser(user2);
db.dropUser(user3);
db.dropUser(user4);
db.dropUser(user5);
db.dropUser(user6);
db.dropUser(user7);

