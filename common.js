// Common functionalities to all API functions

const mypath = require ('path');

// Absolute path to the DB root
const root = mypath.resolve(process.env.DB_ROOT);

// Normalise a path as a relative path to root
function normalise(path) {
		// Turn a relative file name into a file name relative to
		// the client root directory, ready for insertion
		clean_path = path.normalize('NFC');
		if (root != "") {
				relative_path = mypath.relative(root,clean_path);
				return relative_path;
		}
		else
				return path.resolve(clean_path);
};
exports.normalise=normalise;

// replace any substring of in the form /dirname/basename by an equivalent file name relative to
// $DB_ROOT
function replace (s) {
		begin=0; end=0;
		res="";
		while (s.length>0) {
				begin=s.indexOf("\"\/");
				if (begin == -1) break;
				res = res + s.slice(0,begin-1);
				s=s.slice(begin+1);
				end=s.indexOf("\"");
				tmp=s.slice(0,end);
				s=s.slice(end+1);
				newtmp=normalise(tmp);
				// console.log("normalising ",tmp, " into ",newtmp);
				res=res + "\"" + newtmp + "\"";
		};
		res=res+s;
		return res;
};
exports.replace=replace;
