FROM ubuntu:18.04
LABEL Decoder "info@decoder.org"
RUN apt update -y && apt install -y python3-pip unzip python3-dev wget git \
        gcc tar cmake g++ autoconf automake autoconf-archive autotools-dev \
        libtool clang software-properties-common nodejs

# install mongodb server
RUN echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-3.4.list
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv BC711F9BA15703C6
RUN apt-get update
RUN apt install -y mongodb-org-server
RUN apt list --installed | grep mongo
RUN mkdir /mongodb-projects

# install mongodb client
RUN apt-get install -y mongodb-org libmongoc-dev
RUN wget -qO- https://deb.nodesource.com/setup_14.x | bash -
# install nodejs
RUN apt-get install -y nodejs
RUN npm install md-2-json --save
RUN npm install text-encoding --save
RUN npm install mongodb --save
RUN npm install assert common fs path --save

# install frama-c
RUN apt install -y pkg-config libcairo2-dev libgnomecanvas2-dev \
        libgtksourceview-3.0-dev libgmp-dev graphviz
RUN add-apt-repository ppa:avsm/ppa && apt update -y && apt install -y opam

RUN opam init --shell-setup --bare --disable-sandboxing -y
SHELL ["/bin/bash", "-l", "-c"]
RUN opam switch -y create 4.08.1
RUN opam install -y depext
RUN opam depext lablgtk3 lablgtk3-sourceview3
RUN opam install -y lablgtk3 lablgtk3-sourceview3
RUN opam install -y zarith alt-ergo ocamlgraph why3 yojson coq
RUN opam pin -y add why3 1.2.1
RUN wget https://frama-c.com/download/frama-c-20.0-Calcium.tar.gz
RUN tar -xzf frama-c-20.0-Calcium.tar.gz
RUN cd frama-c-20.0-Calcium && ./configure --prefix=/root/.opam/4.08.1 && make && make install && cd ..

COPY . /pkm-api
WORKDIR /pkm-api
# install doc-to-asfm
RUN cd doc/doc_to_asfm && rm -fr build && mkdir build && cd build && cmake .. && make -j 4

ENV API=/pkm-api
ENV PKMHOST=localhost
ENV DB_ROOT=mongodb-projects

CMD ["mongod -dbpath /mongodb-projects"]
ENTRYPOINT ["/bin/bash", "-l", "-c"]
# RUN chmod +x start.sh
# ENTRYPOINT ["./start.sh"]
