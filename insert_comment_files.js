"use strict";

const path = require('path');
const MongoClient = require('mongodb').MongoClient;
const assert = require ('assert');
const fs = require ('fs');
const host=process.env.PKMHOST;
const url = 'mongodb://' + host +':27017';
const dbName = 'mydb';
const client = new MongoClient(url,{ useUnifiedTopology: true});


client.connect(function(err) {
		assert.equal(null,err);
		const db = client.db(dbName);
		const collection = db.collection('Comments');
		
		// The comments files are given as arguments.
		// Get list all files from command arguments, using process.argv

		process.argv.forEach(function (val, index, array) {
				if (index>1)
				{
						let rawdocument = fs.readFileSync(val);
						let document = JSON.parse(rawdocument);
						for (let j=0; j<document.comments.length; j++)							
								collection.insertOne(document.comments[j], function(err, res) {
										if (err) {
												console.log(err);
												throw err;
										}
										else {
												console.log("Document inserted: " + val);
												client.close();
										}
								});
				}
		});
		console.log("All comments files have been inserted.");
});
